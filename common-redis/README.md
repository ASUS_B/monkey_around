一） 缓存配置
# 是否开启redis缓存  true开启   false关闭
spring.redis.enable: true
# redis使用的数据库，默认有16个(0-15)，从0开始
spring.redis.database: 0
# ip地址 127.0.0.1
spring.redis.host: 127.0.0.1
# 端口号 6379
spring.redis.port: 6379
# 密码（默认为空）
spring.redis.password: 
# 连接超时时长（毫秒）
spring.redis.timeout: 6000
# 缓存键前缀 默认为 redis
spring.redis.sysName: redis
# 连接池最大连接数（使用负值表示没有限制）
spring.redis.pool.max-active: 1000
# 连接池最大阻塞等待时间（使用负值表示没有限制）
spring.redis.pool.max-wait: -1
# 连接池中的最大空闲连接
spring.redis.pool.max-idle: 10
# 连接池中的最大空闲连接
spring.redis.pool.min-idle: 5



# 基于注解实现 redis 缓存，
    添加/修改缓存 注解为 com.test.cache.annotation.@Cache, 缓存更新为 整体更新。
    清楚缓存      注解为  com.test.cache.annotation.@CacheClear
#示例一：
            @Cache(key ="selectPage{1}")
            public Page<T> selectPage(Page<T> page){ return null;}
#示例二：
             @Cache(key ="selectPage{2.paramNameValuePairs}{2.toString}")
             public Page<T> selectPage( Wrapper<T> wrapper) {}
             此处2.paramNameValuePairs 中2表示 参数wrapper ，2.paramNameValuePairs 获取的是参数wrapper 的属性paramNameValuePairs的值。
             也可以{2.toString} 获取 参数wrapper toString() 方法的值。切记此处，需是 无参方法，避免报错
#示例三：
             @CacheClear(pre=":",key="selectPage",keys={"selecPage1","selecPage1"})
            public void clearTCache(){}
              
            
           
            
            
            
        
            
            
            

