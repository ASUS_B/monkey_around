package com.test.cache.entity;

import com.test.cache.utils.DateUtil;

import java.util.Date;


public class CacheBean {
    /**
     * key
     */
    private String key = "";
    /**
     * 有效期时间
     */
    private String expireTime;

    public CacheBean(String key,Date expireDate) {
        this.key = key;
        if(expireDate!=null){
            this.expireTime = DateUtil.format(expireDate, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        }
    }
    public CacheBean(String key) {
        this.key = key;
            this.expireTime = DateUtil.format( new Date(System.currentTimeMillis() - 2000) , DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
    }
    public CacheBean(String key, String expireTime) {
        this.key = key;
        this.expireTime = expireTime;
    }

    public CacheBean() {

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public void setExpireTime(Date expireDate) {
        if(expireDate!=null) {
            this.expireTime = DateUtil.format(expireDate, DateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        }
    }

}
