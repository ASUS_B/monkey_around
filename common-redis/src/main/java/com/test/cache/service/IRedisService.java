package com.test.cache.service;

import redis.clients.jedis.BinaryClient.LIST_POSITION;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * redis 数据类型
 * 1)String 2）hash  3)list  4)set  5)sorted set
 *
 */
public interface IRedisService {
/* ---------========================获取 key值=====================================----------*/
    /**
     * 通过前缀获取储存在redis中的 key 集合
     * @param pre 前缀
     */
    Set<String> getKeyByPre(String pre);

    /**
     * 判断key是否存在
     * @param key
     * @return true OR false
     */
    Boolean existsKey(String key);

/* ---------========================移除 key值=====================================----------*/
    /**
     * 根据前缀移除key
     * @param pre 一个key 也可以使 string 数组
     * @return 返回删除成功的个数
     */
    Long delKeyByPre(String pre);

    /**
     * 删除指定的key,也可以传入一个包含key的数组
     * @param keys 一个key 也可以使 string 数组
     * @return 返回删除成功的个数
     */
    Long delKeys(String... keys);


     /* ---------========================操作String 类型数据=====================================----------*/
    /**
     * 获取 String 类型数据
     * @param key
     * @return
     */
    String get(String key);
    /**
     * 向redis存入key和value,并释放连接资源
     * 如果key已经存在 则覆盖
     * @param key
     * @param value
     * @return 成功 返回OK 失败返回 0
     */
    String set(String key, String value);
    /**
     * 向redis存入key和value,并释放连接资源
     * 如果key已经存在 则覆盖
     * @param key
     * @param value
     * @param expire 有效时间(秒)
     * @return 成功 返回OK 失败返回 0
     */
    String set(String key, String value, int expire);
    /**
     * 通过key向指定的 字符串后 追加 字符串
     * @param key
     * @param str
     * @return 成功返回 添加后value的长度 失败 返回 添加的 value 的长度 异常返回0L
     */
    Long append(String key, String str);



    /**
     * 设置key 的value 为string类型
     * 如果key已经存在则返回0,nx==> not exist
     * 如果key不存在，则操作内存。
     * @param key
     * @param value
     * @return 成功返回1 如果存在 和 发生异常 返回 0
     */
    Long setnx(String key, String value);

    /**
     * 设置key 对应的值为 string 类型的 value，并指定此键值对应的有效期。
     * @param key
     * @param value
     * @param seconds 单位:秒
     * @return 成功返回OK 失败和异常返回null
     */
    String setex(String key, String value, int seconds);

    /**
     * 通指定 key  和 offset(value的下标)，替换value字符串。
     * 下标从0开始,offset表示从offset下标开始替换
     * 如果替换的字符串长度过小则会这样
     * example:
     * value : bigsea@zto.cn
     * str : abc
     * 从下标7开始替换 则结果为
     * RES : bigsea@abc.cn
     * @param key
     * @param str
     * @param offset 下标位置
     * @return 返回替换后 value 的长度
     */
    Long setrange(String key, int offset, String str);

    /**
     * 通过下标 和key 获取指定下标位置的 value (字符串截取)
     * @param key
     * @param startOffset 开始位置 从0 开始， 负数表示从右边开始截取
     * @param endOffset
     * @return 如果没有返回null
     */
    String getrange(String key, int startOffset, int endOffset);

    /**
     * 通过批量的key获取批量的value
     * @param keys string数组 也可以是一个key
     * @return 成功返回value的集合, 失败返回null的集合 ,异常返回空
     */
    List<String> mget(String... keys);

    /**
     * 批量的设置key:value,可以一个
     * example:
     * obj.mset(new String[]{"key2","value1","key2","value2"})
     * @param keysvalues
     * @return 成功返回OK 失败 异常 返回 null
     */
    String mset(String... keysvalues);

    /**
     * 批量的设置key:value,可以一个key
     * 如果有一个key已经存在，则会失败,操作会回滚
     * example:
     * obj.msetnx(new String[]{"key2","value1","key2","value2"})
     * @param keysvalues
     * @return 成功返回1 失败返回0
     */
    Long msetnx(String... keysvalues);

    /**
     * 设置key的值为 value,并返回旧的value值
     * @param key
     * @param value
     * @return 旧值 如果key不存在 则返回null
     */
    String getset(String key, String value);



    /**
     * 将 key 中储存的数字值增一
     * 如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 INCR 操作(此时key的值为1)，且将key的有效时间设置为长期有效(注意设置 key的有效时间)。
     * @param key
     * @return 加值后的结果
     */
    Long incr(String key);

    /**
     * 将 key 中储存的数字加上指定的 增量值，如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 INCR 操作。
     * @param key
     * @param integer
     * @return
     */
    Long incrBy(String key, Long integer);

    /**
     * 对key的值做减减操作，如果key不存在,,默认从0开始-1，然后再执行 decr，值为 -1
     * @param key
     * @return
     */
    Long decr(String key);

    /**
     * 对key的值做减减操作,减去指定的值
     * @param key
     * @param integer
     * @return
     */
    Long decrBy(String key, Long integer);

    /**
     * 获取指定 key 所储存的字符串值的长度
     * @param key
     * @return 失败返回null
     */
    Long strlen(String key);




/* ---------========================操作 哈希Hash<字符串> 数据类型=====================================----------*/
    /**
     * 通过key 给field设置指定的值,如果key不存在,则先创建
     * @param key
     * @param field 字段(HashMap的key)
     * @param value 值(HashMap的value)
     * @return 如果存在返回0 异常返回null
     */
    Long hset(String key, String field, String value);

    /**
     * 通过key给field设置指定的值,如果key不存在则先创建,如果field已经存在,返回0
     * @param key
     * @param field
     * @param value
     * @return
     */
    Long hsetnx(String key, String field, String value);

    /**
     * 通过key同时设置 hash的多个field
     * @param key
     * @param hash
     * @return 返回OK 异常返回null
     */
    String hmset(String key, Map<String, String> hash);

    /**
     * 通过key 和 field 获取指定的 value
     * @param key
     * @param field
     * @return 没有返回null
     */
    String hget(String key, String field);

    /**
     * 通过key 和 fields 获取指定的value 如果没有对应的value则返回null
     * @param key
     * @param fields 可以使 一个String 也可以是 String数组
     * @return
     */
    List<String> hmget(String key, String... fields);

    /**
     * Redis Hincrby 命令用于为哈希表中的字段值加上指定增量值。
     * 增量也可以为负数，相当于对指定字段进行减法操作。
     * 如果哈希表的 key 不存在，一个新的哈希表被创建并执行 HINCRBY 命令。
     * 如果指定的field不存在，那么在执行命令前，字段的值被初始化为 0 。
     * 对一个储存字符串值的字段执行 HINCRBY 命令将造成一个错误。
     * 本操作的值被限制在 64 位(bit)有符号数字表示之内。
     * @param key
     * @param field
     * @param value
     * @return 执行命令后的value值
     */
    Long hincrby(String key, String field, Long value);

   Double hincrbyfloat(String key, String field,Double value );

    /**
     * 检查 哈希字段field 是否存在。
     *
     * @param key
     * @param field
     * @return 1-如果哈希包含字段。0- field不存在，或key不存在。
     */
    Boolean hexists(String key, String field);

    /**
     * 通过key返回field的数量
     * 返回哈希表 key 中 域field 的数量。
     * @param key
     * @return
     */
    Long hlen(String key);

    /**
     * 通过key 删除指定的 field
     * @param key
     * @param fields 可以是 一个 field 也可以是 一个数组
     * @return
     */
    Long hdel(String key, String... fields);

    /**
     * 通过key返回所有的field
     * @param key
     * @return
     */
    Set<String> hkeys(String key);

    /**
     * 通过key返回所有和key有关的value
     * 返回哈希表 key 中所有域的值。
     *
     * @param key
     * @return
     */
    List<String> hvals(String key);

    /**
     *  返回哈希表中，所有的字段和值。
     * @param key
     * @return
     */
    Map<String, String> hgetall(String key);


/* ---------========================操作 列表List<String> 数据类型=====================================----------*/

    /**
     * 通过key向list头部添加字符串
     * @param key
     * @param strs 可以使一个string 也可以使string数组
     * @return 返回list的value个数
     */
    Long lpush(String key, String... strs);

    /**
     * 通过key向list尾部添加字符串
     * @param key
     * @param strs 可以使一个string 也可以使string数组
     * @return 返回list的value个数
     */
    Long rpush(String key, String... strs);

    /**
     * 通过key在list指定的位置之前或者之后 添加字符串元素
     * 当列表不存在时，被视为空列表，不执行任何操作。
     * 当指定元素不存在于列表中时，不执行任何操作。
     * @param key
     * @param where LIST_POSITION枚举类型( BEFORE, AFTER)
     * @param pivot list里面的value
     * @param value 添加的value
     * @return
     */
    Long linsert(String key, LIST_POSITION where, String pivot, String value);

    /**
     * 通过索引来设置元素的值。
     * 当索引参数超出范围，或对一个空列表进行 LSET 时，返回一个错误。
     * @param key
     * @param index 从0开始
     * @param value
     * @return 成功返回OK
     */
    String lset(String key, Long index, String value);

    /**
     * Lrem 根据参数 COUNT 的值，移除列表中与参数 VALUE 相等的元素。
     * count > 0 : 从表头开始向表尾搜索，移除与 VALUE 相等的元素，数量为 COUNT 。
     * count < 0 : 从表尾开始向表头搜索，移除与 VALUE 相等的元素，数量为 COUNT 的绝对值。
     * count = 0 : 移除表中所有与 VALUE 相等的值。
     * @param key
     * @param count 当count为0时删除全部
     * @param value
     * @return 返回被删除的个数
     */
    Long lrem(String key, long count, String value);

    /**
     *  Ltrim 对一个列表进行修剪(trim)，就是说，让列表只保留指定区间内的元素，不在指定区间之内的元素都将被删除。负数表示从尾部开始
     * @param key
     * @param start
     * @param end
     * @return 成功返回OK
     */
    String ltrim(String key, long start, long end);

    /**
     *移除列表的第一个元素，返回值为移除的元素。
     *
     * @param key
     * @return
     */
    String lpop(String key);

    /**
     * 移除列表的最后一个元素，返回值为移除的元素。
     *
     * @param key
     * @return
     */
    String rpop(String key);

    /**
     * 移除列表的最后一个元素，并将该元素添加到另一个列表并返回。
     *
     * @param srckey 移除的列表
     * @param dstkey 添加的列表
     * @return
     */
    String rpoplpush(String srckey, String dstkey);

    /**
     * 通过索引获取列表中的元素。你也可以使用负数下标，以 -1 表示列表的最后一个元素， -2 表示列表的倒数第二个元素，以此类推。
     * @param key
     * @param index
     * @return
     */
    String lindex(String key, long index);

    /**
     * 通过key返回list的长度
     * @param key
     * @return
     */
    Long llen(String key);

    /**
     * 返回列表中指定区间内的元素，区间以偏移量 START 和 END 指定。
     * 其中 0 表示列表的第一个元素， 1 表示列表的第二个元素，以此类推。
     * 你也可以使用负数下标，以 -1 表示列表的最后一个元素， -2 表示列表的倒数第二个元素，以此类推。
     * @param key
     * @param start
     * @param end
     * @return
     */
    List<String> lrange(String key, long start, long end);


/* ---------========================操作 集合Set<String>  数据类型=====================================----------*/


    /**
     * 通过key向指定的set中添加value
     * @param key
     * @param members 可以是一个String 也可以是一个String数组
     * @return 添加成功的个数
     */
    Long sadd(String key, String... members);

    /**
     * 通过key删除set中对应的value值
     * @param key
     * @param members 可以是一个String 也可以是一个String数组
     * @return 删除的个数
     */
    Long srem(String key, String... members);

    /**
     * 移除集合中的指定 key 的 一个随机元素，移除后会返回移除的元素。
     * @param key
     * @return
     */
    String spop(String key);

    /**
     * 返回给定key 对应集合之间的差集。不存在的集合 key 将视为空集。
     * 差集 的结果来自于 第一个key
     * @param keys 可以使一个string 则返回set中所有的value 也可以是string数组
     * @return
     */
    Set<String> sdiff(String... keys);

    /**
     * 将给定集合之间的差集存储在指定的集合中。如果指定的集合 key 已存在，则会被覆盖。
     *
     * @param dstkey 差集存入(指定集合)的 key
     * @param keys   可以使一个string 则返回set中所有的value 也可以是string数组
     * @return
     */
    Long sdiffstore(String dstkey, String... keys);

    /**
     * <p>
     * 通过key获取指定set中的交集
     * </p>
     *
     * @param keys 可以使一个string 也可以是一个string数组
     * @return
     */
    Set<String> sinter(String... keys);

    /**
     * <p>
     * 通过key获取指定set中的交集 并将结果存入新的set中
     * </p>
     *
     * @param dstkey
     * @param keys   可以使一个string 也可以是一个string数组
     * @return
     */
    Long sinterstore(String dstkey, String... keys);

    /**
     * <p>
     * 通过key返回所有set的并集
     * </p>
     *
     * @param keys 可以使一个string 也可以是一个string数组
     * @return
     */
    Set<String> sunion(String... keys);

    /**
     * <p>
     * 通过key返回所有set的并集,并存入到新的set中
     * </p>
     *
     * @param dstkey
     * @param keys   可以使一个string 也可以是一个string数组
     * @return
     */
    Long sunionstore(String dstkey, String... keys);

    /**
     * <p>
     * 通过key将set中的value移除并添加到第二个set中
     * </p>
     *
     * @param srckey 需要移除的
     * @param dstkey 添加的
     * @param member set中的value
     * @return
     */
    Long smove(String srckey, String dstkey, String member);

    /**
     * <p>
     * 通过key获取set中value的个数
     * </p>
     *
     * @param key
     * @return
     */
    Long scard(String key);

    /**
     * <p>
     * 通过key判断value是否是set中的元素
     * </p>
     *
     * @param key
     * @param member
     * @return
     */
    Boolean sismember(String key, String member);

    /**
     * <p>
     * 通过key获取set中随机的value,不删除元素
     * </p>
     *
     * @param key
     * @return
     */
    String srandmember(String key);

    /**
     * <p>
     * 通过key获取set中所有的value
     * </p>
     *
     * @param key
     * @return
     */
    Set<String> smembers(String key);

    /**
     * <p>
     * 通过key向zset中添加value,score,其中score就是用来排序的
     * </p>
     * <p>
     * 如果该value已经存在则根据score更新元素
     * </p>
     *
     * @param key
     * @param score
     * @param member
     * @return
     */
    Long zadd(String key, double score, String member);

    /**
     * <p>
     * 通过key删除在zset中指定的value
     * </p>
     *
     * @param key
     * @param members 可以使一个string 也可以是一个string数组
     * @return
     */
    Long zrem(String key, String... members);

    /**
     * <p>
     * 通过key增加该zset中value的score的值
     * </p>
     *
     * @param key
     * @param score
     * @param member
     * @return
     */
    Double zincrby(String key, double score, String member);

    /**
     * <p>
     * 通过key返回zset中value的排名
     * </p>
     * <p>
     * 下标从小到大排序
     * </p>
     *
     * @param key
     * @param member
     * @return
     */
    Long zrank(String key, String member);

    /**
     * <p>
     * 通过key返回zset中value的排名
     * </p>
     * <p>
     * 下标从大到小排序
     * </p>
     *
     * @param key
     * @param member
     * @return
     */
    Long zrevrank(String key, String member);

    /**
     * <p>
     * 通过key将获取score从start到end中zset的value
     * </p>
     * <p>
     * socre从大到小排序
     * </p>
     * <p>
     * 当start为0 end为-1时返回全部
     * </p>
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    Set<String> zrevrange(String key, long start, long end);

    /**
     * <p>
     * 通过key返回指定score内zset中的value
     * </p>
     *
     * @param key
     * @param max
     * @param min
     * @return
     */
    Set<String> zrangebyscore(String key, String max, String min);

    /**
     * <p>
     * 通过key返回指定score内zset中的value
     * </p>
     *
     * @param key
     * @param max
     * @param min
     * @return
     */
    Set<String> zrangeByScore(String key, double max, double min);

    /**
     * <p>
     * 返回指定区间内zset中value的数量
     * </p>
     *
     * @param key
     * @param min
     * @param max
     * @return
     */
    Long zcount(String key, String min, String max);

    /**
     * <p>
     * 通过key返回zset中的value个数
     * </p>
     *
     * @param key
     * @return
     */
    Long zcard(String key);

    /**
     * <p>
     * 通过key获取zset中value的score值
     * </p>
     *
     * @param key
     * @param member
     * @return
     */
    Double zscore(String key, String member);

    /**
     * <p>
     * 通过key删除给定区间内的元素
     * </p>
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    Long zremrangeByRank(String key, long start, long end);

    /**
     * <p>
     * 通过key删除指定score内的元素
     * </p>
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    Long zremrangeByScore(String key, double start, double end);

    /**
     * <p>
     * 返回满足pattern表达式的所有key
     * </p>
     * <p>
     * keys(*)
     * </p>
     * <p>
     * 返回所有的key
     * </p>
     *
     * @param pattern
     * @return
     */
    Set<String> keys(String pattern);

    /**
     * <p>
     * 通过key判断值得类型
     * </p>
     *
     * @param key
     * @return
     */
    String type(String key);

    /**
     * 获取key过期时间
     *
     * @param key
     * @return
     */
    Date getExpireDate(String key);
    /**
     * 清空数据
     * 20180515
     * @return
     */
    void flushDB();


    void setList(String key, List<?> list);

    List<?> getList(String key);
    /**
     * 根据key值 获取对象
     * @param key
     * @return
     */
    Object getObject(String key);
    /**
     * 根据key值 存储对象
     * @param key
     * @param obj
     * @return
     */
    void setObject(String key,Object obj);
    /**
     * 根据key值 存储对象
     * key值已存在，则 不操作内存。
     * @param key
     * @param value
     * @param expire 有效时间，秒
     * @return "OK" 操作内存成功
     */
    String setNxObject(String key,Object value,int expire);
    /**
     * 根据key值 存储对象
     * 当key值不纯在，则 不操作内存。
     * @param key
     * @param value
     * @param expire 有效时间，秒
     * @return "OK" 操作内存成功
     */
    String setXxObject(String key,Object value,int expire);

    /**
     * 获取key的 有效期秒数
     * @param key
     * @return {-1:key过期 , -2 : key 不存在}
     */
    Long ttl(String key);
}