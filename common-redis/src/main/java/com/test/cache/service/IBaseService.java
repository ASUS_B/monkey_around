package com.test.cache.service;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ASUS_B
 * @since 2017-11-07
 */
public interface IBaseService<T> extends IService<T> {
    /**
     * 清除 基类T 实现类的 所有redis 缓存
     */
    public void clearTCache();

    /**
     * 根据方法名称 清除 方法缓存
     * @param methodName 方法名称，methodName中 {} 不解析
     */
    public void clearPreKeyBaseCache(String methodName);

    /**
     * 根据前缀 清除 方法缓存
     * @param preKey 缓存前缀，preKey中 {} 不解析
     */
    public void clearPreKeyCache(String preKey);
}
