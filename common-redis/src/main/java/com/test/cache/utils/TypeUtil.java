package com.test.cache.utils;

import java.io.*;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TypeUtil {
    /**
     *
     * @param methodReturnType  类型
     * @return typeTag{1-基础数据类型，2-List , 3-Hash}
     */
    public static int judgeMethodReturnType(Type methodReturnType) {
        if(methodReturnType instanceof Class){
            Class cl = ((Class) methodReturnType);
            if (cl.equals(String.class)) {
                return 1;
            }
            if (cl.equals(Integer.class)||cl.equals(int.class)) {
                return 1;
            }
            if (cl.equals(Long.class)||cl.equals(long.class)) {
                return 1;
            }
            if (cl.equals(Double.class)||cl.equals(double.class)) {
                return 1;
            }
            if (cl.equals(Float.class)||cl.equals(float.class)) {
                return 1;
            }
            if (cl.equals(BigDecimal.class)) {
                return 1;
            }
        }
        if (methodReturnType instanceof ParameterizedType) {
            /* 参数化类型，即泛型；例如：List<T>、Map<K,V>等带有参数化的对象;  */
            ParameterizedType pt = (ParameterizedType) methodReturnType;
            Type rawType = pt.getRawType();
            if (((Class) rawType).isAssignableFrom(List.class)) {
                return 2;
            }
        }
        if (methodReturnType instanceof TypeVariable) {
            /* 类型变量，即泛型中的变量；例如：T、K、V等变量，可以表示任何类；在这需要强调的是，
            TypeVariable代表着泛型中的变量，而ParameterizedType则代表整个泛型 */
//            TypeVariable tType = (TypeVariable) methodReturnType;
        }

        if (methodReturnType instanceof GenericArrayType) {
//            /* 泛型数组类型，用来描述ParameterizedType、TypeVariable类型的数组；即List<T>[] 、T[]等 */
//            GenericArrayType arrayType = (GenericArrayType) methodReturnType;
        }
        return 0;
    }

}
