package com.test.cache.utils;

/**
 * Created by Administrator on 2019/1/18.
 */
public class ConstantUtil {
    public static final String COLON = ":";
    public static final String SERVICE_SUFFIX = "ServiceImpl";
    public static final String DOT = ".";
    public static final String DEFAULT_REDIS_KEY_PREFIX = "redis";
    /* ====================正则表达式==========================*/
    public static final String REGEX_GROUP_1 = "\\{\\d+\\.?[\\w]*\\}";
    public static final String REGEX_GROUP_2 = "\\.";
    public static final String REGEX_GROUP_3 = "(:)\\1+";

    public static final byte[] XX_BYTE = "XX".getBytes();
    public static final byte[] EX_BYTE = "EX".getBytes();
    public static final byte[] NX_BYTE = "NX".getBytes();
    public static final byte[] PX_BYTE = "PX".getBytes();


}
