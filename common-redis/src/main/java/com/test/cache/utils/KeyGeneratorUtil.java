package com.test.cache.utils;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class KeyGeneratorUtil {

    /**
     * @param key
     * @param parameterTypes 参数类型数组
     * @param arguments      参数值数组
     * @return  不以 :开头的字符串
     */
    public static String generatorKey(String key, Class<?>[] parameterTypes, Object[] arguments) {
        if (StringUtils.isNotBlank(key)) {
            boolean tag =false;
            key = key.replace("{", ":{");
            Pattern pattern = Pattern.compile(ConstantUtil.REGEX_GROUP_1);
            Matcher matcher = pattern.matcher(key);
            while (matcher.find()) { // 找到了 匹配的字符串
                String tmp = matcher.group();
                tmp = tmp.substring(1,tmp.length()-1);
                String[] express = tmp.split(ConstantUtil.REGEX_GROUP_2);
                String i = express[0];
                int index = Integer.parseInt(i) - 1;
                Object value = arguments[index];
                if (parameterTypes[index].isAssignableFrom(List.class)) {
                    List result = (List) arguments[index];
                    value = result.get(0);
                }
                if (value == null || "null".equals(value)) {
                    value = "";
                }
                if (express.length > 1) {
                    String field = express[1];
                    try {
                        // 获取字段的值
                        value = ReflectionUtils.getFieldValue(value, field);
                    }catch (IllegalArgumentException ex ){
                        // 没有字段 ，调用方法， 空参()
                        value= ReflectionUtils.getMethodValueByName(value,field);
                    }
                }
                key = key.replace("{" + tmp + "}", value.toString());
            }
        }
        return key;
    }


    //首字母转小写
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0))) {
            return s;
        }
        else {
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }


    public static String getEntityClassName(ProceedingJoinPoint invocation){
        //获取切点所在类的类名 (全路径 类名 eg:com.test.service.impl.SysUserServiceImpl)
        String pointClassName = invocation.getTarget().getClass().getName();
        int lastPointIndex=pointClassName.lastIndexOf(ConstantUtil.DOT);
        if(lastPointIndex!=-1){
            //获取 实现类名称 eg: SysUserServiceImpl
            pointClassName = pointClassName.substring(lastPointIndex+1);
            int serviceImplIndex = pointClassName.lastIndexOf(ConstantUtil.SERVICE_SUFFIX);
            if(serviceImplIndex!= -1){
                //获取 基类名称 eg: SysUser
                pointClassName =pointClassName.substring(0,serviceImplIndex);
            }
        }
        //基类名称 首字母转小写
        pointClassName= KeyGeneratorUtil.toLowerCaseFirstOne(pointClassName);
        return  pointClassName;
    }


    /**
     * 替换掉 key中 连续重复的 ：
     * eg: 将 test:::123 替换为 test:123
     *
     * 正则表达式 为  "(.)\\1+"   时，为替连续重复的 字符
     * 例如： 将 test11:::122433 替换为 test1:1243
     * @param key
     * @return
     */
    public static String replaceRepeat(String key){
        Pattern pattern = Pattern.compile(ConstantUtil.REGEX_GROUP_3);
        Matcher matcher = pattern.matcher(key);
        //sb 缓冲区作为存储替换后的结果
        StringBuffer sb = new StringBuffer();
        //用正则表达式的find函数去判断，有没有匹配的结果集
        while (matcher.find()) {
            //match.group(0) 是匹配的字符串，比如111234445466中的111
            //match.group(1) 是匹配的字符串的单个字符，比如111234445466中的111中的1
            String repeat = matcher.group(1);
            //appendReplacement用第二个参数去替换匹配成功的子串，并把结果存放在sb中，前面未匹配成功的也会放进去，后面的未匹配成功的不会放进去。例如：11123444546634 最后会替换成(1)23(4)54(6)
            matcher.appendReplacement(sb, repeat );
        }
        //把后面未匹配成功的附加到sb上,例如：11123444546634 最后会替换成(1)23(4)54(6)34
        matcher.appendTail(sb);
       return  sb.toString();
    }
}

