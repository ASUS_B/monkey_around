package com.test.cache.annotation;



import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 开启缓存 注解
 */
@Retention(RetentionPolicy.RUNTIME)
// 在运行时可以获取
@Target(value = {ElementType.METHOD, ElementType.TYPE})
// 作用到类，方法，接口上等
public @interface Cache {
    /**
     * 缓存key
     * eg:
     *  @Cache("{1}")
     *  public ButtonConfig selectById(Serializable id){ return id;}
     */
    public String key() default "";

    /**
     * 过期时间 (默认值为120 分钟)
     */
    public int expire() default 120;
}
