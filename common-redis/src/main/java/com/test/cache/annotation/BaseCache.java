package com.test.cache.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 开启缓存
 * 需要实现类 后缀 为   **.**.****ServiceImpl
 */
@Retention(RetentionPolicy.RUNTIME)
// 在运行时可以获取
@Target(value = {ElementType.METHOD, ElementType.TYPE})
// 作用到类，方法，接口上等
public @interface BaseCache {
    /**
     * 缓存key menu_{0.id}{1}_type
     */
    public String key() default "";

    /**
     * 过期时间 默认120分钟
     */
    public int expire() default 120;//
}
