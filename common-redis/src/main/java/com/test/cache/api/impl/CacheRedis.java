package com.test.cache.api.impl;

import com.alibaba.fastjson.JSON;
import com.test.cache.api.CacheAPI;
import com.test.cache.config.RedisConfig;
import com.test.cache.entity.CacheBean;
import com.test.cache.service.IRedisService;
import com.test.cache.utils.ConstantUtil;
import com.test.cache.utils.KeyGeneratorUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * redis缓存
 */
@Service
public class CacheRedis implements CacheAPI {
    @Autowired
    private RedisConfig redisConfig;

    @Autowired
    private IRedisService redisCacheService;

    @Override
    public String get(String key) {
        if (!isEnabled()) {
            return null;
        }
        if(StringUtils.isBlank(key)){
            return null;
        }
        key = addSys(key);
        return redisCacheService.get(key);
    }

    @Override
    public void set(String key, Object value, int expireMin) {
        set(key, value, expireMin, "");
    }

    @Override
    public Long remove(String key) {
        if (!isEnabled()) {
            return 0L;
        }
        if(StringUtils.isBlank(key)) {
            return 0L;
        }
        try {
            key = addSys(key);
            redisCacheService.delKeys(key);
        } catch (Exception e) {
            return 0L;
        }
        return 1L;
    }

    @Override
    public Long remove(String... keys) {
        if (!isEnabled()) {
            return null;
        }
        try {
            for (int i = 0; i < keys.length; i++) {
                remove(keys[i]);
            }
        } catch (Exception e) {
            return 0L;
        }
        return 1L;
    }

    @Override
    public Long removeByPre(String pre) {
        if (!isEnabled()) {
            return 0L;
        }
        if(StringUtils.isBlank(pre)) {
            return 0L;
        }
        if(pre.equals(redisConfig.getSysName())){
            redisCacheService.flushDB();
            return 0L;
        }
        try {
            String key = addSys(pre);
            Set<String> result = redisCacheService.getKeyByPre(key);
            redisCacheService.delKeys(result.toArray(new String[]{}));
            redisCacheService.delKeyByPre(key);
        } catch (Exception e) {
            return 0L;
        }
        return 1L;
    }



    /**
     * 加入系统前缀 默认为 redis
     */
    @Override
    public String addSys(String key) {
        String result = key;
        String sys = redisConfig.getSysName();
        if(StringUtils.isBlank(sys)){
            sys = ConstantUtil.DEFAULT_REDIS_KEY_PREFIX;
        }
        if (key.startsWith(sys)) {
            result = key;
        }
        else {
           result = sys + ":" + key;
        }
        /**
         * 替换掉 key 中 重复出现的 :
         * 列如 : 将 test:::123 替换为 test:123
         */
        result = KeyGeneratorUtil.replaceRepeat(result);
        return result;
    }

    @Override
    public void set(String key, Object value, int expireMin, String desc) {
        if (StringUtils.isBlank(key) || value == null
                || StringUtils.isBlank(value.toString())) {
            return;
        }
        if (!isEnabled()) {
            return;
        }
        String realValue = "";
        if (value instanceof String) {
            realValue = value.toString();
        } else {
            realValue = JSON.toJSONString(value, false);
        }
         redisCacheService.set(addSys(key), realValue, expireMin * 60);
    }



    @Override
    public boolean isEnabled() {
        return Boolean.parseBoolean(redisConfig.getEnable());
    }


    @Override
    public Object getObject(String key){
        if (!isEnabled()) {
            return null;
        }
        if(StringUtils.isBlank(key)){
            return null;
        }
        key = addSys(key);
        return redisCacheService.getObject(key);
    }

    @Override
    public void setObject(String key, Object value, int expireMin) {
        if (StringUtils.isBlank(key) || value == null ) {
            return;
        }
        if (!isEnabled()) {
            return;
        }
        key = addSys(key);
        int expireSecond = expireMin * 60;
       String result = redisCacheService.setNxObject(key,value,expireSecond);
        if(!judgeResultIsOk(result)){
            redisCacheService.setXxObject(key,value,expireSecond);
        }
    }


    private static boolean judgeResultIsOk(String result){
        if(result!=null){
            if("OK".equals(result) || "ok".equals(result)){
                return true;
            }
        }
        return false;
    }

    /**
     * 根据key值 获取有效期
     * @param key
     * @return
     */
    @Override
    public CacheBean getCacheExpireTime(String key){
        if (StringUtils.isBlank(key)) {
            return new CacheBean(key);
        }
        if (!isEnabled()) {
            return new CacheBean(key);
        }
        key = addSys(key);
        Date expireDate = redisCacheService.getExpireDate(key);
        return  new CacheBean(key,expireDate);
    }

    @Override
    public Long getCacheTTL(String key){
        Long result = -2L;
        if (StringUtils.isBlank(key)) {
            return result;
        }
        if (!isEnabled()) {
            return result;
        }
        key = addSys(key);
        return  redisCacheService.ttl(key);
    }

    /**
     * 获取所有缓存对象信息
     */
    @Override
    public List<CacheBean> getKeysExpireTime(String pre){
        List<CacheBean> resultList = new ArrayList<CacheBean>();
        if (!isEnabled()) {
            return resultList;
        }
        if(StringUtils.isNotBlank(pre)){
            pre = addSys(pre);
        }
        if(StringUtils.isBlank(pre)){
            pre = addSys("");
        }
        Set<String> result = redisCacheService.getKeyByPre(pre);
        if (result == null) {
            return resultList;
        }
        Iterator<String> it = result.iterator();
        String key = "";
        CacheBean cache = null;
        while (it.hasNext()) {
            cache = null;
            key = it.next();
            resultList.add( new CacheBean(key,redisCacheService.getExpireDate(key)));
        }
        return  resultList;
    }
}
