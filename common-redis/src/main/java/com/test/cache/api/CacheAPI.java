package com.test.cache.api;


import com.test.cache.entity.CacheBean;

import java.util.List;

/**
 * 缓存API
 * <p/>
 * 解决问题：
 *
 * @since 1.7
 */
public interface CacheAPI {
    /**
     * 传入key获取缓存json，需要用fastjson转换为对象
     *
     * @param key
     * @return
     * @author Ace
     * @date 2017年5月12日
     */
    public String get(String key);

    /**
     * 保存缓存
     *
     * @param key
     * @param value
     * @param expireMin  有效时间(单位:分钟)
     * @author Ace
     * @date 2017年5月12日
     */
    public void set(String key, Object value, int expireMin);

    /**
     * 保存缓存
     *
     * @param key
     * @param value
     * @param expireMin
     * @param desc
     * @author Ace
     * @date 2017年5月12日
     */
    public void set(String key, Object value, int expireMin, String desc);

    /**
     * 移除单个缓存
     *
     * @param key
     * @return
     * @author Ace
     * @date 2017年5月12日
     */
    public Long remove(String key);

    /**
     * 移除多个缓存
     *
     * @param keys
     * @return
     * @author Ace
     * @date 2017年5月12日
     */
    public Long remove(String... keys);

    /**
     * 按前缀移除缓存
     *
     * @param pre
     * @return
     * @author Ace
     * @date 2017年5月12日
     */
    public Long removeByPre(String pre);

    /**
     * 是否启用缓存
     *
     * @return
     * @author Ace
     * @date 2017年5月12日
     */
    public boolean isEnabled();

    /**
     * 加入缓存 前缀
     *
     * @param key
     * @return
     * @author Ace
     * @date 2017年5月12日
     */
    public String addSys(String key);



    /**
     * 根据key值 存储对象
     * key值已存在，则 不操作内存。
     * @param key
     * @param value
     * @param expireMin 有效时间，分
     * @return
     */
    public void setObject(String key,Object value,int expireMin);

    /**
     * 根据key值 获取对象
     * @param key
     * @return
     */
   public Object getObject(String key);

    /**
     * 查询缓存 key的过期时间
     * @param key
     */
    public CacheBean getCacheExpireTime(String key);


    public Long getCacheTTL(String key);

    /**
     * 根据缓存前缀 查询缓存 key的过期时间
     */
    public List<CacheBean> getKeysExpireTime(String pre);
}
