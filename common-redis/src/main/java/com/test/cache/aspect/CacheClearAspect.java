package com.test.cache.aspect;

import com.test.cache.annotation.CacheClear;
import com.test.cache.api.CacheAPI;
import com.test.cache.utils.KeyGeneratorUtil;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.lang.reflect.Method;

/**
 * 清除缓存注解 切面
 */
@Aspect
@Service
public class CacheClearAspect {
    @Autowired
    private CacheAPI cacheAPI;

    @Pointcut("@annotation(com.test.cache.annotation.CacheClear)")
    public void aspect() {
    }

    @Around("aspect()&&@annotation(anno)")
    public Object interceptor(ProceedingJoinPoint invocation, CacheClear anno)
            throws Throwable {
        // 获取 切点 方法签名对象
        MethodSignature signature = (MethodSignature) invocation.getSignature();
        // 获取 切点 方法对象
        Method method = signature.getMethod();
        // 获取 方法的所有参数类型
        Class<?>[] parameterTypes = method.getParameterTypes();
        // 返回目标方法的参数
        Object[] arguments = invocation.getArgs();
        // 生成 key
        String key =null;
        boolean tag = true;
        if (StringUtils.isNotBlank(anno.key())) {
            key =  KeyGeneratorUtil.generatorKey(anno.key(), parameterTypes, arguments);
            cacheAPI.remove(key);
            tag =false;
        }
        if (StringUtils.isNotBlank(anno.pre())) {
            key =  KeyGeneratorUtil.generatorKey(anno.pre(), parameterTypes, arguments);
            cacheAPI.removeByPre(key);
            tag =false;
        }
        if (anno.keys().length > 1) {
            for (String tmp : anno.keys()) {
                tmp =  KeyGeneratorUtil.generatorKey(anno.pre(), parameterTypes, arguments);
                cacheAPI.removeByPre(tmp);
            }
            tag =false;
        }
        if(tag){
            String pointEntityClassName = KeyGeneratorUtil.getEntityClassName(invocation);
            cacheAPI.removeByPre(pointEntityClassName);
        }
        return invocation.proceed();
    }

}
