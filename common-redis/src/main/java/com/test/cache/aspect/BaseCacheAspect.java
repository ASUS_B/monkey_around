package com.test.cache.aspect;

import com.alibaba.fastjson.JSONObject;
import com.test.cache.annotation.BaseCache;
import com.test.cache.api.CacheAPI;
import com.test.cache.utils.ConstantUtil;
import com.test.cache.utils.KeyGeneratorUtil;
import com.test.cache.utils.TypeUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;

/**
 * 缓存开启注解拦截
 * 注:  生成key时  自动拼接 基类名称
 * eg: news:selectById{1}  其中 news: 为自动拼接内容
 *
 * @since 1.7
 */
@Aspect
@Service
public class BaseCacheAspect {
    @Autowired
    private CacheAPI cacheAPI;
    protected Logger log = Logger.getLogger(this.getClass());

    @Pointcut("@annotation(com.test.cache.annotation.BaseCache)")
    public void aspect() {
    }

    @Around("aspect()&&@annotation(anno)")
    public Object interceptor(ProceedingJoinPoint invocation, BaseCache anno)
            throws Throwable {
        Object result = null;
        // 获取 切点 方法签名对象
        MethodSignature signature = (MethodSignature) invocation.getSignature();
        // 获取 切点 方法对象
        Method method = signature.getMethod();

        // 获取 方法的所有参数类型
        Class<?>[] parameterTypes = method.getParameterTypes();
        // 返回目标方法的参数
        Object[] arguments = invocation.getArgs();
        // 生成 key
        String pointEntityClassName = KeyGeneratorUtil.getEntityClassName(invocation);
        // 获取方法名称
//        String methodName = method.getName();
        String  key = KeyGeneratorUtil.generatorKey(pointEntityClassName+ConstantUtil.COLON+anno.key(), parameterTypes, arguments);
       // 获取方法返回类型
        Type methodReturnType = method.getGenericReturnType();
        //判断是否为String 类型
        int typeInt = TypeUtil.judgeMethodReturnType(methodReturnType);
        try {
            if (typeInt == 1) {
                String value = cacheAPI.get(key);
                if (value != null) {
                    if (methodReturnType == String.class) {
                        return value;
                    }
                    result = JSONObject.parseObject(value, methodReturnType);
                }
            }
            if (typeInt != 1) {
                result = cacheAPI.getObject(key);
                if (result != null) {
                    if (result instanceof List) {
                        // redis自带 List 数据类型， 自动存储为链表，取数组第一个
                        List<?> dataList = (List<?>) result;
                        result = dataList.get(0);
                    }
                }
            }
        } catch (Exception e) {
            log.error("获取缓存失败：" + key, e);
        } finally {
            if (result == null) {
                result = invocation.proceed();
                if (result != null && StringUtils.isNotBlank(key)) {
                    // 如果返回参数为List类型
                    if (typeInt == 1) {
                        cacheAPI.set(key, result, anno.expire());
                    }
                    if (typeInt != 1) {
                        cacheAPI.setObject(key, result, anno.expire());
                    }
                }
            }
        }
        return result;
    }

}
