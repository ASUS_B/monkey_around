package com.test.cache.aspect;

import com.test.cache.annotation.BaseCacheClear;
import com.test.cache.api.CacheAPI;
import com.test.cache.utils.ConstantUtil;
import com.test.cache.utils.KeyGeneratorUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 清除缓存注解拦截
 * 注：清除时,key，key,pre, 字符串 都需加 : 在前
 * eg: @BaseCacheClear(pre=":"),@BaseCacheClear(key=":selectById")，@BaseCacheClear(keys={":selectById",":selectPage"})
 *
 * @since 1.7
 */
@Aspect
@Service
public class BaseCacheClearAspect {
    @Autowired
    private CacheAPI cacheAPI;
    protected Logger log = Logger.getLogger(this.getClass());

    @Pointcut("@annotation(com.test.cache.annotation.BaseCacheClear)")
    public void aspect() {
    }

    @Around("aspect()&&@annotation(anno)")
    public Object interceptor(ProceedingJoinPoint invocation, BaseCacheClear anno)
            throws Throwable {
        String pointEntityClassName = KeyGeneratorUtil.getEntityClassName(invocation);

        MethodSignature signature = (MethodSignature) invocation.getSignature();
        Method method = signature.getMethod();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Object[] arguments = invocation.getArgs();
        String key = "";
        boolean tag = true;
        if (StringUtils.isNotBlank(anno.key())) {
            cacheAPI.remove(pointEntityClassName+ConstantUtil.COLON+ anno.key());
            tag= false;
        }
        if (StringUtils.isNotBlank(anno.pre())) {
            cacheAPI.removeByPre(pointEntityClassName+ConstantUtil.COLON+ anno.pre());
            tag= false;
        }
        if (anno.keys().length > 1) {
            for (String tmp : anno.keys()) {
                tmp =  KeyGeneratorUtil.generatorKey(pointEntityClassName+ConstantUtil.COLON+ tmp, parameterTypes, arguments);
                cacheAPI.removeByPre(tmp);
            }
            tag= false;
        }
        if(tag){
            cacheAPI.removeByPre(pointEntityClassName);
        }
        return invocation.proceed();
    }



}
