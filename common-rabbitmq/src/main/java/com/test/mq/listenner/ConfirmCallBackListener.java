package com.test.mq.listenner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.stereotype.Component;

/**
 * 消息确认 监听器
 * 只确认消息是否正确到达 Exchange 中。
 * 如果消息没有到exchange,则confirm回调,ack=false
 * 如果消息到达exchange,则confirm回调,ack=true
 */
@Component
public class ConfirmCallBackListener implements RabbitTemplate.ConfirmCallback {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        logger.info("消息是否 到达交换器确认消息提示------------------------------------");
        if(correlationData!=null) {
            logger.info("correlationData(消息id)==  " + correlationData.getId());
        }
        logger.info("消息是否到达交换器(ack)==  "+ack);
        if(!ack){
            logger.info("造成原因为(cause)==   "+cause);
        }
        logger.info("------------------------------------------------------------------------");
    }
}
