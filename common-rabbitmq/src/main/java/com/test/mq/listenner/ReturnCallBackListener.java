package com.test.mq.listenner;

import com.test.mq.consumer.DirectConsumerTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 *  消息发送失败返回   监听器
 *   消息没有正确到达队 列时触发回调，如果正确到达队列不执行
 *   需要同时设置 CachingConnectionFactory.publisherReturns=true 和 RabbitTemplate.mandatory=true 才有效
 * exchange到queue成功,则不回调return
 * exchange到queue失败,则回调return(需设置 RabbitTemplate.mandatory=true,否则不回回调,消息就丢了)
 */
@Component
public class ReturnCallBackListener implements RabbitTemplate.ReturnCallback {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        logger.info("消息从交换器(exchange)到达队列(routingKey)失败 提示------------------------------------");
        logger.info("消息为=="+new String(message.getBody()));
        if(replyCode==312&& "NO_ROUTE".equals( replyText )){
            logger.info("失败原因为： 没有找到匹配的 消息队列");
        }else{
            logger.info("replyCode=="+replyCode);
            logger.info("replyText=="+replyText);
        }
        logger.info("------------------------------------------------------------------------------");
    }
}
