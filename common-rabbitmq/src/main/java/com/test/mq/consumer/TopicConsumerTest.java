package com.test.mq.consumer;

import com.rabbitmq.client.Channel;
import com.test.mq.constant.MQConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * rabbit消息消费测试类
 * Created by Administrator on 2018/3/16.
 */
@Component
public class TopicConsumerTest {
    private Logger logger = LoggerFactory.getLogger(TopicConsumerTest.class);
    /**
     * 订阅 队列1 消息
     */
    @RabbitListener(queues = MQConstant.TOPIC_MQ_QUEUE_1)
    public void subscribeQueue1_1(String message, Channel channel){
        logger.info("订阅队列1_1 接收到 "+MQConstant.TOPIC_MQ_QUEUE_1+" 的消息:   "+message+", 接收时间:  " +System.currentTimeMillis());
    }

    /**
     * 订阅 队列1 消息
     */
    @RabbitListener(queues = MQConstant.TOPIC_MQ_QUEUE_1)
    public void subscribeQueue1_2(String message){
        logger.info("订阅队列1_2 接收到 "+MQConstant.TOPIC_MQ_QUEUE_1+" 的消息:   "+message+", 接收时间:  " +System.currentTimeMillis());
    }
    /**
     * 订阅 队列2 消息
     */
    @RabbitListener(queues = MQConstant.TOPIC_MQ_QUEUE_2)
    public void subscribeQueue2(String message){
        logger.info("订阅队列2_1 接收到 "+MQConstant.TOPIC_MQ_QUEUE_2+" 的消息:   "+message+", 接收时间:  " +System.currentTimeMillis());
    }


}
