package com.test.mq.consumer;

import com.rabbitmq.client.Channel;
import com.test.mq.constant.MQConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * rabbit消息消费测试类
 * Created by Administrator on 2018/3/16.
 */
@Component
public class DirectConsumerTest {
    private Logger logger = LoggerFactory.getLogger(DirectConsumerTest.class);
    /**
     * 订阅 队列1 消息
     */
    @RabbitListener(queues = MQConstant.DIRECT_MQ_QUEUE_1)
    public void subscribeQueue1_1(Message message, Channel channel){
        Long messageDeliveryTag = null;
//        try {
            messageDeliveryTag = message.getMessageProperties().getDeliveryTag();
            logger.info("订阅队列1_1 接收到 "+MQConstant.DIRECT_MQ_QUEUE_1+" 的消息"+messageDeliveryTag+":   "+new String(message.getBody())+", 接收时间:  " +System.currentTimeMillis());
            //告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
/*
        message.getMessageProperties().getDeliveryTag() 消息标签，根据消息数量 ，依次+1 ， 同一条消息的标签一致。
        multiple 为false时，确认 当前 message.getMessageProperties().getDeliveryTag() 对应的这条消息，已收到。
                 为true时，确认 当前 message.getMessageProperties().getDeliveryTag() 对应的这条消息 及  标签小于 message.getMessageProperties().getDeliveryTag() 的消息已收到。
        对同一消息的重复确认，或者对不存在的消息的确认，会产生 IO 异常，导致信道关闭。
        channel.basicAck( message.getMessageProperties().getDeliveryTag() , false );  // 确认消息已收到

        如果不确认消息收到，只要程序还在运行，这3条消息就一直是 unacked(未确认) 状态，无法被 RabbitMQ 重新发送。
        由于 RabbitMQ 消息消费 并没有超时机制，也就是说，程序不重启，消息就永远是 unacked(未确认) 状态。
        当Consumer(消费者)程序关闭后，消息才会恢复  Ready(已准备/待发送) 状态。
 */
//            channel.basicAck( messageDeliveryTag , false );
//        } catch (IOException e) {
                /*  取消  消息确认
                    channel.basicReject( message.getMessageProperties().getDeliveryTag() ,boolean requeue );
                    requeue 值为true时 ，表示 将该消息重新放回队列头。值为 false 表示放弃这条消息

                    批量取消确认
                    第一个参数deliveryTag：发布的每一条消息都会获得一个唯一的deliveryTag，deliveryTag在channel范围内是唯一的
                    第二个参数multiple：批量确认标志。如果值为true，包含本条消息在内的、所有比该消息deliveryTag值小的 消息都被拒绝了（除了已经被 ack 的以外）;如果值为false，只拒绝本条消息
                    第三个参数requeue：表示如何处理这条消息，如果值为true，则重新放入RabbitMQ的发送队列，如果值为false，则通知RabbitMQ销毁这条消息，消息不回队列
                    channel.basicNack ( long deliveryTag, boolean multiple, boolean requeue );
                */
//            try {
//                channel.basicReject( messageDeliveryTag ,true);
//            }catch (IOException ex){
//                ex.printStackTrace();
//            }
//        }

    }

    /**
     * 订阅 队列1 消息
     */
//    @RabbitListener(queues = MQConstant.DIRECT_MQ_QUEUE_1)
    public void subscribeQueue1_2(String message, Channel channel){
        logger.info("订阅队列1_2 接收到 "+MQConstant.DIRECT_MQ_QUEUE_1+" 的消息:   "+message+", 接收时间:  " +System.currentTimeMillis());
    }
    /**
     * 订阅 队列2 消息
     */
    @RabbitListener(queues = MQConstant.DIRECT_MQ_QUEUE_2)
    public void subscribeQueue2(String message, Channel channel){
        logger.info("订阅队列2_1 接收到 "+MQConstant.DIRECT_MQ_QUEUE_2+" 的消息:   "+message+", 接收时间:  " +System.currentTimeMillis());
    }


}
