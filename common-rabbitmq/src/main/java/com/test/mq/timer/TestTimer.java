package com.test.mq.timer;

import com.test.mq.constant.MQConstant;
import com.test.mq.producer.DirectProducerTest;
import com.test.mq.producer.FanoutProducerTest;
import com.test.mq.producer.TopicProducerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 用于测试   队列消息发送
 */
@Component
public class TestTimer {

    @Autowired
    DirectProducerTest directProducer;
    @Autowired
    TopicProducerTest topicProducer;
    @Autowired
    FanoutProducerTest  fanoutProducer;
    @Scheduled(cron="*/5 * * * * ?") //每5秒执行一次,这是cron表达式
    public void statusCheck() {
        String message1 = "好嗨哦！！"+System.currentTimeMillis();
        String message2 = "嗨尼玛个逼 ！！"+System.currentTimeMillis();
//        directProducer.Queue1PushMessage(message1);

        // 测试 错误的交换器名称  发送消息  ConfirmCallBackListener 中 ack==false ，cause==channel error; protocol method: #method<channel.close>(reply-code=404, reply-text=NOT_FOUND - no exchange 'test44341' in vhost '/', class-id=60, method-id=40)
//        directProducer.pushMessageByExchangeAndRoutingKey("test44341",MQConstant.DIRECT_MQ_QUEUE_1,message1);
        //  测试 消息到到交换器后 没有到达队列  ReturnCallBackListener 消息 返回提示
//        directProducer.pushMessageByExchangeAndRoutingKey(MQConstant.DEFAULT_EXCHANGE_KEY,"queuqessda1238",message2);


//        topicProducer.Queue2PushMessage(message2);
//        topicProducer.testWildcardPushMessage1( "asus.pms.scanda.iot",message1);
        topicProducer.testWildcardPushMessage1("asus.pms.scanda",message2);


//        fanoutProducer.Queue1PushMessage(message1);
    }



}
