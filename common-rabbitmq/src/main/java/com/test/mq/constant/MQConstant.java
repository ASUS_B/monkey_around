package com.test.mq.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ljb on 2017/11/15.
 * Rabbit消息队列相关常量
 * 每个消息队列一个模块调用，
 * 不然mq会自动进行负载处理
 */
public class MQConstant {

    /*---------------- DirectExchange ----------------*/
    //exchange(交换机) name
    public static final String DEFAULT_EXCHANGE_KEY = "direct_exchange_key";
    // 队列1  name
    public static final String DIRECT_MQ_QUEUE_1 = "direct_queue_1";
    // 队列2  name
    public static final String DIRECT_MQ_QUEUE_2 = "direct_queue_2";

//    DirectExchange 交换机 默认 路由键routingkey 即为 队列名称


    /*---------------- TopicExchange ----------------*/

    //exchange(交换机) name
    public static final String TOPIC_EXCHANGE_KEY = "topic_exchange_key";
    // 队列1 名
    public static final String TOPIC_MQ_QUEUE_1 = "topic_queue_1";
    // 队列2  name
    public static final String TOPIC_MQ_QUEUE_2 = "topic_queue_2";
    /*
    TopicExchange 采用通配符， 路由键必须是一串字符，用句号（.） 隔开，eg:  asus.eu.ems  or asus.eu
    TopicExchange 对key进行模式匹配后进行投递，符号”#”匹配一个或多个词，符号”*”匹配正好一个词。
    例如”abc.#”匹配”abc.def.ghi”，”abc.*”只匹配”abc.def”
     */
    // 队列1 routingkey
    public static final String TOPIC_MQ_QUEUE_1_KEY = "asus.pms.*";
    // 队列2 routingkey
    public static final String TOPIC_MQ_QUEUE_2_KEY = "asus.pms.#";


    /*---------------- FanoutExchange  头信息交换器 ----------------*/
    //exchange(交换机) name
    public static final String FANOUT_EXCHANGE_KEY = "fanout_exchange_key";
    // 队列1 名
    public static final String FANOUT_MQ_QUEUE_1 = "fanout_queue_1";
    // 队列2  name
    public static final String FANOUT_MQ_QUEUE_2 = "fanout_queue_2";






    /*---------------- HeadersExchange  头信息交换器 ----------------*/
    //exchange(交换机) name
    public static final String HEADERS_EXCHANGE_KEY = "headers_exchange_key";
    // 队列1 名
    public static final String HEADERS_MQ_QUEUE_1 = "headers_queue_1";
    // 队列2  name
    public static final String HEADERS_MQ_QUEUE_2 = "headers_queue_2";

    // routingkey
    public static final Map<String,Object> HEADERS_MQ_HEARS= new HashMap<String,Object>(){
        {put("name","asus");put("pswd","123456");put("x-match","all");}
    };

}
