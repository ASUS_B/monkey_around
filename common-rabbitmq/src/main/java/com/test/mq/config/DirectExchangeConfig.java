package com.test.mq.config;

import com.test.mq.constant.MQConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 直连交换器   默认 交换器 通道 均为 持久化
 *  一般的消息队列服务有3个概念：发消息者、队列、收消息者。
 *  而 rabbitMq 在 发消息者 和 队列 之间加入了一个  交换器(Exchange)。
 *  发消息者 将消息 发送给 交换器(Exchange)， 交换器根据调度策略再把消息再给队列
 */
@Configuration
public class DirectExchangeConfig {

    /**
     * 交换机(Exchange) 用于转发消息,但是它不会做存储,如果没有 队列(Queue) bind 到 Exchange 的话，它会直接丢弃掉 Producer 发送过来的消息
     * 针对消费者配置交换机
     FanoutExchange:  将消息分发到所有的绑定队列，无 路由键(routingkey) 的概念
     HeadersExchange：通过添加属性key-value匹配
     DirectExchange(默认交换机): 按照 路由键(routingkey) 分发到指定队列。在绑定时设定一个 routing_key, 消息的routing_key 匹配时, 才会被交换器投送到绑定的队列中去.
     TopicExchange:多关键字匹配
     */
    @Bean
    public DirectExchange defaultExchange() {
        return new DirectExchange(MQConstant.DEFAULT_EXCHANGE_KEY);
    }
/*======================         示例队列一             ===========================*/

    /**
     * 注册 rabbitMq故障消息 sms短信转发 队列
     * @return
     */
    @Bean
    public Queue registerDirectQueue1() {
        return new Queue(MQConstant.DIRECT_MQ_QUEUE_1, true); //默认durable=true,让队列持久
    }

    /**
     * 将交换机和消息队列绑定  ,并设置队列的路由键 ==》with(routingKey)  设置路由键
     * DirectExchangeRoutingKeyConfigurer.with(关联routingKey)====》DirectExchange需要按照routingkey分发到指定队列
     * @return
     */
    @Bean
    public Binding BindingDirectQueue1() {
        return BindingBuilder.bind(registerDirectQueue1()).to(defaultExchange()).with(MQConstant.DIRECT_MQ_QUEUE_1);
    }



/*======================         示例队列二            ===========================*/


    /**
     * 注册 rabbitMq故障消息 websocket转发 队列
     * @return
     */
    @Bean
    public Queue registerDirectQueue2() {
        return new Queue(MQConstant.DIRECT_MQ_QUEUE_2, true); //默认durable=true,让队列持久
    }

    /**
     * 将交换机和消息队列绑定 ,并设置队列的路由键 ==》with(routingKey)  设置路由键
     * DirectExchangeRoutingKeyConfigurer.with(关联routingKey)====》DirectExchange需要按照routingkey分发到指定队列
     *
     * @return
     */
    @Bean
    public Binding BindingDirectQueue2() {
        return BindingBuilder.bind(registerDirectQueue2()).to(defaultExchange()).with(MQConstant.DIRECT_MQ_QUEUE_2);
    }
}
