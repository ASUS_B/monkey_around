package com.test.mq.producer;

import com.test.mq.constant.MQConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * rabbit消息发送测试类
 * Created by Administrator on 2018/3/16.
 *  rabbitTemplate.send(message);   //发消息，参数类型为org.springframework.amqp.core.Message
 *  rabbitTemplate.convertAndSend(object); //转换并发送消息。 将参数对象转换为org.springframework.amqp.core.Message后发送
 *  rabbitTemplate.convertSendAndReceive(message) //转换并发送消息,且等待消息者返回响应消息。
 */
@Component
public class DirectProducerTest {
    private Logger logger = LoggerFactory.getLogger(DirectProducerTest.class);
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息
     * @param message
     */
    public void Queue1PushMessage(Object message){
        logger.info(MQConstant.DIRECT_MQ_QUEUE_1+"   发送消息:   { "+message+" }");
        rabbitTemplate.convertAndSend( MQConstant.DEFAULT_EXCHANGE_KEY,MQConstant.DIRECT_MQ_QUEUE_1,message );
    }

    /**
     * 发送消息
     * @param message
     */
    public void Queue2PushMessage(Object message){
        logger.info(MQConstant.DIRECT_MQ_QUEUE_2+"   发送消息:   { "+message+" }");
        rabbitTemplate.convertAndSend(MQConstant.DEFAULT_EXCHANGE_KEY, MQConstant.DIRECT_MQ_QUEUE_2,message);
    }


    /**
     * 发送消息
     * @param message
     */
    public void pushMessageByExchangeAndRoutingKey(String exchange,String routingKey,Object message){
        String messageId = UUID.randomUUID().toString();
        logger.info("交换机("+exchange+")队列("+routingKey+")   发送消息:   { "+message+" ,"+messageId+"}");
        try {
            rabbitTemplate.convertAndSend(exchange, routingKey, message, new CorrelationData(messageId));
        }catch (AmqpException ex){
            logger.info("消息{ " +message+" } 发送失败！！！！！！！！！！！！！！！！！！！！！！！！！！！");
            ex.printStackTrace();
        }
    }
}
