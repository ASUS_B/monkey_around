package com.test.mq.producer;

import com.test.mq.constant.MQConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * rabbit消息发送测试类
 */
@Component
public class TopicProducerTest {
    private Logger logger = LoggerFactory.getLogger(TopicProducerTest.class);
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息
     * @param message
     */
    public void Queue1PushMessage(Object message){
        logger.info(MQConstant.TOPIC_MQ_QUEUE_1+"  发送消息:   { "+message+" }");
        this.rabbitTemplate.convertAndSend(MQConstant.TOPIC_EXCHANGE_KEY, MQConstant.TOPIC_MQ_QUEUE_1_KEY,message);
    }

    /**
     * 发送消息
     * @param message
     */
    public void Queue2PushMessage(Object message){
        logger.info(MQConstant.TOPIC_MQ_QUEUE_2+"   发送消息:   { "+message+" }");
        this.rabbitTemplate.convertAndSend(MQConstant.TOPIC_EXCHANGE_KEY, MQConstant.TOPIC_MQ_QUEUE_2_KEY,message);
    }

    /**
     * 测试 通配符发送消息
     * @param message
     */
    public void testWildcardPushMessage1(String wildcardRoutingkey,Object message){
        logger.info("根据通配符("+wildcardRoutingkey+")  发送消息:   { "+message+" }");
        this.rabbitTemplate.convertAndSend(MQConstant.TOPIC_EXCHANGE_KEY,wildcardRoutingkey,message);
    }
    /**
     * 测试 通配符发送消息
     * @param message
     */
    public void testWildcardPushMessage2(String wildcardRoutingkey,Object message){
        logger.info("根据通配符("+wildcardRoutingkey+")  发送消息:   { "+message+" }");
        this.rabbitTemplate.convertAndSend(MQConstant.TOPIC_EXCHANGE_KEY,wildcardRoutingkey,message);
    }

}
