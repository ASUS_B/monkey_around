package com.test.mq.producer;

import com.test.mq.constant.MQConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * rabbit消息发送测试类
 */
@Component
public class FanoutProducerTest {
    private Logger logger = LoggerFactory.getLogger(FanoutProducerTest.class);
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息
     * @param message
     */
    public void Queue1PushMessage(Object message){
        logger.info(MQConstant.FANOUT_MQ_QUEUE_1+"  发送消息:   { "+message+" }");
        this.rabbitTemplate.convertAndSend(MQConstant.FANOUT_EXCHANGE_KEY, "",message);
    }



}
