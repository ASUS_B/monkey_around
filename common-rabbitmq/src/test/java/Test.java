import com.test.mq.producer.DirectProducerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Administrator on 2019/1/21.
 */
@ComponentScan("com.test")
@SpringBootApplication
@EnableScheduling //添加注解启用定时器
public class Test {

    public static void main(String[] args){
        SpringApplication.run(Test.class, args);
   }
}
