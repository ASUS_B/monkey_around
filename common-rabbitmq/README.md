#mq配置
# 配置ip(默认127.0.0.1)
    spring.rabbitmq.host: localhost
# 配置端口(默认5672)
    spring.rabbitmq.port: 5672
# 配置账号(默认guest)
    spring.rabbitmq.username: guest
# 配置密码(默认guest)
    spring.rabbitmq.password: guest
#开启 消息确认ack
    #开启消息发送确认
    spring.rabbitmq.publisher-confirms=true
    # 开启发送失败退回
    spring.rabbitmq.publisher-returns=true
    # 开启ACK
    spring.rabbitmq.listener.direct.acknowledge-mode=manual
    spring.rabbitmq.listener.simple.acknowledge-mode=manual
    
#测试描述
 1)检查mq配置
 2)打开com.test.mq.timer.TestTimer 的 @Component 注解，使spring托管,使用 定时器 测试发送消息
 3)运行与 main 同级的 test.java.Test 类
 
#使用描述
    切记 屏蔽com.test.mq.timer.TestTimer 的 @Component 注解
    
    
    
# 报错日志显示    
    2019-01-23 11:34:30.018 ERROR 280 --- [ 127.0.0.1:5678] o.s.a.r.c.CachingConnectionFactory       : Channel shutdown: channel error; protocol method: #method<channel.close>(reply-code=406, reply-text=PRECONDITION_FAILED - unknown delivery tag 1, class-id=60, method-id=80)
    2019-01-23 11:34:31.011  WARN 280 --- [cTaskExecutor-5] o.s.a.r.l.SimpleMessageListenerContainer : Consumer raised exception, processing can restart if the connection factory supports it
    com.rabbitmq.client.ShutdownSignalException: channel error; protocol method: #method<channel.close>(reply-code=406, reply-text=PRECONDITION_FAILED - unknown delivery tag 1, class-id=60, method-id=80)

 