import com.test.weixin.constant.WxConstant;
import com.test.weixin.util.WxSignUtil;
import com.test.weixin.util.WxXmlUtil;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import org.springframework.boot.SpringApplication;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by Administrator on 2019/2/13.
 */
public class Test {

    public static void main(String[] args){

        SortedMap<String, Object> parameters = new TreeMap<String, Object>();
        parameters.put("appid", WxConstant.APP_ID);//商户id
        parameters.put("mch_id", WxConstant.MCH_ID);//商户收款账号
        // 商品描述 需传入应用市场上的APP名字-实际商品名称  示例：天天爱消除-游戏充值。
        parameters.put("body", WxConstant.ORDER_NAME);
        //商户订单编号
        parameters.put("out_trade_no", "123");
        // 订单总金额，单位为分，
        parameters.put("total_fee", 159);
        //终端IP ,调用微信支付API的机器IP,支持IPV4和IPV6两种格式的IP地址。
        parameters.put("spbill_create_ip", "192.168.1.163");
        //交易类型(trade_type)  JSAPI--JSAPI支付(或小程序支付)、NATIVE--Native支付、APP--app支付、MWEB--H5支付、MICROPAY--付款码支付(单独的支付接口)
        parameters.put("trade_type", "APP");
        // 接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。
        parameters.put("notify_url", WxConstant.NOTIFY_URL);
        // 随机字符串-用于保证签名 不可预测
        parameters.put("nonce_str", String.valueOf(System.currentTimeMillis()));
        // 生成 签名
        String sign = WxSignUtil.createSign( parameters );
        parameters.put("sign", sign);

        String xml = WxXmlUtil.mapToXmlStr(parameters);
        System.out.println(xml);
        SortedMap<String, Object> parameter1 = WxXmlUtil.xmlStrToMap(xml);
        System.out.println(parameter1.toString());


        String xml2 = "<xml><sign>B26D8E0CCC6FAFD363DF6E5CAC51FA58</sign><body></body></xml>";
        SortedMap<String, Object> parameter2 = WxXmlUtil.xmlStrToMap(xml2);
        System.out.println(parameter2.toString());
    }
}
