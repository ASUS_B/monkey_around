package com.test.weixin.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by Administrator on 2019/2/13.
 */
public class WxXmlUtil {

    /**
     * 将map 转换为请求的 xml字符串
     * @param parameters
     * @return 带CDATA标签(用于说明数据不被XML解析器解析)的xml字符串
     */
    public static String mapToXmlStr(SortedMap<String, String> parameters) {
        //请求xml参数
        StringBuffer requestStr = new StringBuffer("<xml>");
//        requestStr.append("<appid><![CDATA[");
//        requestStr.append(parameters.get("appid"));
//        requestStr.append("]]></appid>");
        for(Map.Entry<String, String> entry : parameters.entrySet()){
            requestStr.append("<").append(entry.getKey()).append("><![CDATA[");
            requestStr.append(entry.getValue());
            requestStr.append("]]></").append(entry.getKey()).append(">");
        }
        requestStr.append("</xml>");
        return requestStr.toString();
    }

    /**
     * 将 xml字符串 转换为Map
     * @param xmlStr xml字符串
     * @return
     */
    public static SortedMap<String, String> xmlStrToMap(String xmlStr) {
        SortedMap<String, String> xmlMap = new TreeMap<String, String>();
        try {
            xmlStr = new String(xmlStr.getBytes("ISO-8859-1"), "utf-8");
            Document doc = DocumentHelper.parseText(xmlStr);//将xml转为dom对象
            Element root = doc.getRootElement();//获取根节点
            List<Element> elements = root.elements();//获取这个子节点里面的所有子元素，也可以element.elements("userList")指定获取子元素
            for (Element obj : elements) {  //遍历子元素
                xmlMap.put(obj.getName(), obj.getTextTrim());
            }
        }catch (UnsupportedEncodingException ex){
            ex.printStackTrace();
        }catch (DocumentException ex){
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlMap;
    }

    /**
     * 获取xml编码字符集
     *
     * @param strxml
     * @return
     */
    public static String getXMLEncoding(String strxml){
        try {
            Document doc = DocumentHelper.parseText(strxml);//将xml转为dom对象
            return doc.getXMLEncoding();
        }catch (DocumentException ex){
            ex.printStackTrace();
        }
        return null;
    }
    public static String notifyXmlStr(){
         return  "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"+ "<return_msg><![CDATA[OK]]></return_msg>" + "</xml>";
    }

    public static byte[] notifyXmlBytes(){
        return notifyXmlStr().getBytes();
    }
}
