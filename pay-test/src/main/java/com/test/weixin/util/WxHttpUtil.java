package com.test.weixin.util;

        import org.apache.http.*;
        import org.apache.http.client.ClientProtocolException;
        import org.apache.http.client.HttpClient;
        import org.apache.http.client.methods.HttpPost;
        import org.apache.http.entity.StringEntity;
        import org.apache.http.impl.client.HttpClients;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;

        import java.io.*;

/**
 * Created by Administrator on 2019/2/13.
 */
public class WxHttpUtil {
    private static Logger logger = LoggerFactory.getLogger(WxHttpUtil.class);

    /**
     * post请求
     * @param url
     * @param param
     * @return
     */
    public static String doPost(String url, String param){
        String returnXml = "";
        HttpClient httpclient =  HttpClients.createDefault();
        HttpPost httppost = new HttpPost(url);
        StringEntity myEntity = new StringEntity(param, Consts.UTF_8);
        httppost.addHeader("Content-Type", "text/xml");
        httppost.setEntity(myEntity);
        HttpResponse response;
        try {
            response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            InputStreamReader reader = new InputStreamReader(resEntity.getContent(), "UTF8");
            char[] buff = new char[1024];
            int length = 0;
            while ((length = reader.read(buff)) != -1) {
                returnXml += new String(buff, 0, length);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnXml;
    }


    public static String resultSuccess() {
        return "<xml>\n" +
                "  <return_code><![CDATA[SUCCESS]]></return_code>\n" +
                "  <return_msg><![CDATA[OK]]></return_msg>\n" +
                "</xml>";
    }
}
