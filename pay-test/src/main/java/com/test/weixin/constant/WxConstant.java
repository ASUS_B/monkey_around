package com.test.weixin.constant;

/**
 * Created by Administrator on 2019/2/13.
 */
public class WxConstant {
    /**
     * 真实环境 统一下单接口路径
     */
    public static final String WX_PAY_URL ="https://api.mch.weixin.qq.com/pay/unifiedorder";

    /**
     * 查询订单 支付状态
     */
    public static final String WX_ORDER_QUERY_URL ="https://api.mch.weixin.qq.com/pay/orderquery";
    /**
     * 测试环境 统一下单接口路径
     */
    public static final String WX_PAY_URL_TEST ="https://api.mch.weixin.qq.com/sandboxnew/pay/unifiedorder";

    public static final String SUCCESS ="SUCCESS";
    /**
     * appid是微信公众账号或开放平台APP的    唯一标识
     * 在公众平台申请公众账号或者在开放平台申请APP账号后，微信会自动分配对应的appid，用于标识该应用.
     */
    public static String APP_ID ="";
    /**
     * AppSecret是APPID对应的               接口密码
     * 用于获取接口调用凭证access_token时使用。
     */
    public static String  APP_SECRET ="";
    /**
     * 商户申请微信支付后，由微信支付分配的  商户收款账号。
     */
    public static String MCH_ID ="";
    /**
     *  生成sign签名的 密钥
     */
    public static String KEY ="";

    /**
     * 接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。
     */
    public static String NOTIFY_URL ="";

    public static String ORDER_NAME ="";





}
